import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
} from 'react-router-dom'
import { createBrowserHistory } from "history";
import './App.css';
import CreateUser from './containers/SignupScreen';
import HomePage from './containers/HomePage';
import LoginUser from './containers/LoginScreen';


function App() {
  const history = createBrowserHistory();
  return (
    <Router history={history}>
      <div>
        <nav>
          <ul>
            <li>
              <Link to='/create'>SignUp</Link>
            </li>
            <li>
              <Link to='/login'>Login</Link>
            </li>
          </ul>
        </nav>

        <Switch>
          <Route path='/login' exact component={LoginUser} />
          <Route path='/' exact component={HomePage} />
          <Route path='/create' exact component={CreateUser} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
