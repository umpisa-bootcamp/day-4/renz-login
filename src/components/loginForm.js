import React, {useState, useEffect} from 'react';
import { NavLink } from 'react-router-dom';


const UserLogin = ({
}) => {
    const [state, setState] = useState({
        email: '',
        password: '',
    })

    const onChangeField = (event, field) => {
        setState({
            ...state,
            [field]: event.target.value
        })
    }

    return (
        <div>
             <p>Email</p>
            <input
                type="text"
                onChange={e => onChangeField(e, 'name')}
                defaultValue={state.name}
            />
            <p>Password</p>
            <input
                type="password"
                onChange={e => onChangeField(e, 'password')}
                defaultValue={state.password}
            />
            <br />
            <p>
            <NavLink to="/"> <button onClick={() => {}}>Login</button> </NavLink>
            </p>
        </div>
    )
}

export default UserLogin;