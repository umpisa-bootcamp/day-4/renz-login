import React, {useState, useEffect} from 'react';

// import Modal from 'react-awesome-modal';

const UserCreateForm = ({
    onCreate,
    user,
}) => {
    const [state, setState] = useState({
        email: '',
        password: '',
        name: '',
        visible: false,
    })

    useEffect(() => {
        setState({
            ...state,
            email: user.email,
            password: user.password,
            name: user.name
        })
    }, [])

    const onChangeField = (event, field) => {
        setState({
            ...state,
            [field]: event.target.value
        })
    }

    const onSubmit = (data) => {onCreate(data)}

    // copenModal() {
    //     this.setState({
    //         visible : false
    //     });
    // }

    // closeModal() {
    //     this.setState({
    //         visible : false
    //     });
    // }
    
    return (
        <div>
            <input
                type="text"
                onChange={e => onChangeField(e, 'name')}
                defaultValue={state.name}
            />
            <p>Name</p>

            <input
                type="text"
                onChange={e => onChangeField(e, 'email')}
                defaultValue={state.email}
            />
             <p>Email</p>
            <input
                type="password"
                onChange={e => onChangeField(e, 'password')}
                defaultValue={state.password}
            />
            <p>Password</p>
            <br />
            <p>
                <button  onClick={() => onSubmit(state)}>Create</button>
            </p>
            {/* onClickmodal={() => this.openModal()} */}
            {/* <Modal visible={this.state.visible} width="400" height="300" effect="fadeInUp" onClickAway={() => this.closeModal()}>
                    <div>
                        <h1>Welcome!</h1>
                        <p>Logging in...</p>
                        <a href="javascript:void(0);" onClickClose={() => this.closeModal()}>Close</a>
                    </div>
            </Modal> */}
        </div>
    )
}

export default UserCreateForm;