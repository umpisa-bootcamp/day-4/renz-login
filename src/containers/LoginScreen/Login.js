import React, { useState } from 'react';
import {useHistory} from 'react-router-dom';
import UserLogin from '../../components/loginForm';

function LoginUser() {
    const history = useHistory()
    const historyState = history.location.state
    const [state, setState] = useState({
      user: historyState ? historyState.user : {},
    })

  return (
    <div style={{textAlign: 'center'}}>
      <UserLogin />
    </div>
  );
}

export default LoginUser;