import React, { useState } from 'react';
import {useHistory} from 'react-router-dom';

import UserCreateForm from '../../components/signupForm';

function CreateUser() {
    const history = useHistory()
    const historyState = history.location.state
    const [state, setState] = useState({
      user: historyState ? historyState.user : {},
    })

const createUser = (data) => {
    fetch('http://localhost:3001/v1/auth', {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(data)
    }).then(() => history.push('/home'))
  }

  return (
    <div style={{textAlign: 'center'}}>
        <UserCreateForm
        user={state.user}
        onCreate={createUser} />
      
    </div>
  );
}

export default CreateUser;