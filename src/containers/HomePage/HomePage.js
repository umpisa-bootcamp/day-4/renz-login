import React, {useState, useEffect} from 'react';

function HomePage() {
  const [state, setState] = useState({
    index: true,
    users: [],
  })

  const fetchUser = () => {
    fetch('http://localhost:3001/v1/auth')
      .then(res => res.json())
      .then(res => setState({...state, users: res.data}))
  }

  useEffect(() => {
    fetchUser();
  }, [])

  return (
    <div className="App">
      <header className="App-header">
        Hello, 
      {/* <ShowUser users={users.name} */}
      </header>
    </div>
  );
}

export default HomePage;
